package condition;

import java.util.Scanner;

//常用，如:語音
public class SwitchCase {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("請輸入0~9");
		int num = scan.nextInt();
		double f = 9.9;
		System.out.println(num);
		
//		(1)不能判斷小數。只能byte、char、short、int、long、string
//		switch(f) {
		switch(num) {
//		(4)不管default放哪個位置，都是其他條件不符時才會走
		default:
			System.out.println("default");
			break;
//		(5)沒有break，就不會跳出	
		case 0:
			System.out.println("帳戶服務");
		case 1:
			System.out.println("信用卡服務");
			break;
		case 9:
			System.out.println("轉接專人服務");
			break;
//		(2)只能等於
//		case > 8:
		
//		(3)不能重複
//		case 1:
		}
		
		scan.close();
	}
}
